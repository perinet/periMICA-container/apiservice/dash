# apiservice dash

The apiservice dash collects samples of all elements within an application.
It provides the collected samples together with the belonging element name
at the resource `/dash/sample` with server-sent events (sse).

## Example

### js

```js
const eventSource = new EventSource("/dash/sample");

eventSource.onmessage = function(event) {
    data = JSON.parse(event.data);
    console.log(event.data);
}
```


```json
{
  "element_name":"/current/sink",
  "payload":{
    "timestamp":{
      "incarnation":10,
      "system_time":4232322323
    },
    "data":{
      "distance":{
        "value":24.535,
        "unit":"mm"
      }
    }
  }
}
```

## Dual License

This software is by default licensed via the GNU Affero General Public License
version 3. However it is also available with a commercial license on request
(https://perinet.io/contact).
