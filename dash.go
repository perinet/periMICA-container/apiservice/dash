/*
 * Copyright (c) Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */
package dash

import (
	"context"
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/jackc/pgerrcode"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/stdlib"
	"golang.org/x/sys/unix"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/sanity-io/litter"

	"gitlab.com/perinet/generic/apiservice/mqttclient"
	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/periHttp"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
)

type stats_type struct {
	DBSize           uint64 `json:"db_size"`
	FreeSpace        uint64 `json:"free_space"`
	WriteRateLastDay uint64 `json:"write_rate_last_day"`
}

type sample_type struct {
	Time    time.Time       `json:"time"`
	Element string          `json:"element"`
	Message json.RawMessage `json:"message"`
}

// type db_elements_type struct {
// 	id          uint32
// 	name        string
// 	json_schema *string
// }

type db_samples_type struct {
	//id      uint32
	time    time.Time
	element string
	message json.RawMessage
}

type db_datapoints_type struct {
	id         uint32
	element    string
	alias_name *string
}

type db_views_type struct {
	id                  uint32
	name                *string
	datapoint_selection []uint32
}

const (
	endpoint          = "/dash/sample"
	db_write_interval = 1000 * time.Millisecond
)

var (
	logger              log.Logger = *log.Default()
	subscriber_endpoint mqttclient.MQTTClientSubscriberEndpoint
	msgHandlerLock      sync.RWMutex
	db                  *sql.DB
	samples             *[]sample_type = new([]sample_type)
	samplesLock         sync.RWMutex
	application_name    string
)

func init() {
	logger.SetPrefix("apiservice-dashboard: ")
	logger.Print("starting")

	// init database
	var err error
	connStr := "user=postgres sslmode=disable"
	retry := true

	for retry {
		retry = false
		db, err = sql.Open("pgx", connStr)
		if err != nil {
			logger.Println("SQL connection error:", err)
		} else {
			_, err := db.Exec(`CREATE DATABASE charts;`)
			// TODO remove then done
			// err = nil
			if err != nil {
				if sqlErr, ok := err.(*pgconn.PgError); ok {
					if sqlErr.Code == pgerrcode.DuplicateDatabase {
						logger.Println("info: database already exist")
						db.Close()

						connStr := "user=postgres dbname=charts sslmode=disable"
						db, err = sql.Open("pgx", connStr)
						if err != nil {
							logger.Println("SQL connection error:", err)
						}
					} else {
						logger.Println("error creating database, sql error:", sqlErr)
						db.Close()
					}
				} else {
					if strings.Contains(err.Error(), "connect: no such file or director") || strings.Contains(err.Error(), "server error: FATAL: the database system is starting up") {
						logger.Println("error creating database, retry, error:", err)
						db.Close()
						time.Sleep(1 * time.Second)
						retry = true
					} else {
						logger.Println("error creating database, error:", err)
						db.Close()
					}
				}
			} else {
				logger.Println("info: database successfully created")
				db.Close()

				connStr := "user=postgres dbname=charts sslmode=disable"
				db, err = sql.Open("pgx", connStr)
				// db, err = sql.Open("postgres", connStr)
				if err != nil {
					logger.Println("SQL connection error:", err)
				} else {

					// REMOVE then DONE
					// _, err := db.Exec(`DROP FUNCTION IF EXISTS get_element_name; DROP FUNCTION IF EXISTS get_element_id; DROP TABLE IF EXISTS views; DROP TABLE IF EXISTS datapoints; DROP TABLE IF EXISTS samples; DROP TABLE IF EXISTS elements;`)
					// if err != nil {
					// 	logger.Println("SQL CLEANUP error:", err)
					// }
					row := db.QueryRow(`SELECT current_database();`)
					var dbresult string
					if err := row.Scan(&dbresult); err != nil {
						logger.Println("SELECT error:", err)
					}
					logger.Println("current_database:", dbresult)

					result, err := db.Exec(`
					  /*
					  TODO slow table sync
					  CREATE TABLE elements (
						  id SERIAL PRIMARY KEY,
						  name TEXT NOT NULL UNIQUE,
						  json_schema TEXT
					  );*/
					  CREATE TABLE samples (
						  /*id SERIAL PRIMARY KEY,*/
						  time TIMESTAMP NOT NULL,
						  element TEXT NOT NULL,
						  message JSON NOT NULL,
						  /* minute TIMESTAMP, */
						  PRIMARY KEY(element, time)
						  /*element INTEGER NOT NULL,
						  FOREIGN KEY (element) REFERENCES elements (id)*/
					  );
					  /* CREATE INDEX samples_time_element ON samples("time") INCLUDE (element); */
					  /* CREATE INDEX samples_time ON samples USING BRIN("time"); */
					  /* CREATE INDEX samples_minute ON samples(element, minute) INCLUDE ("time", message); requires normal TIMESTAMP */
					  CREATE TABLE datapoints (
						  id SERIAL PRIMARY KEY,
						  element TEXT NOT NULL,
						  /*json path or module to handle message*/
						  alias_name TEXT
						  /*analog/digital, unit and other metadata*/
						  /*FOREIGN KEY (element) REFERENCES elements (id)*/
					  );
					  CREATE TABLE views (
						  /*id SERIAL PRIMARY KEY,*/
						  name VARCHAR(255) NOT NULL,
						  datapoint_selection INTEGER[] NOT NULL
						  /*foreign key on array currently not supported in postgres FOREIGN KEY (EACH ELEMENT OF datapoint_selection) REFERENCES datapoints (id)*/
					  );
					  /*
					  CREATE FUNCTION get_element_id(element_name TEXT) RETURNS INTEGER AS $$
						  INSERT INTO elements (name) VALUES (element_name) ON CONFLICT (name) DO UPDATE SET name=EXCLUDED.name RETURNING id;
					  $$ LANGUAGE SQL;
					  CREATE FUNCTION get_element_name(element_id INTEGER) RETURNS TEXT AS $$
						  SELECT name FROM elements WHERE id=element_id;
					  $$ LANGUAGE SQL;*/
				  `)
					if err != nil {
						logger.Println("SQL query error:", err)
					} else {
						logger.Println(litter.Sdump("SQL query result:", result))
						logger.Println(litter.Sdump("SQL db:", db))
						logger.Println(litter.Sdump("SQL db:", db.Stats()))

						rows, _ := db.Query("SELECT schemaname, tablename FROM pg_catalog.pg_tables;")

						for rows.Next() {
							var schemaname string
							var tablename string
							if err := rows.Scan(&schemaname, &tablename); err != nil {
								logger.Println("SQL query error:", err)
								continue
							}
							if schemaname == "public" {
								logger.Println(schemaname, tablename)
							}
						}
					}
				}
			}
		}
	}

	Resubscribe()

	go sample_handler()
}

func Resubscribe() {
	data := intHttp.Get(node.NodeInfoGet, nil)
	var nodeInfo node.NodeInfo
	err := json.Unmarshal(data, &nodeInfo)
	if err != nil {
		logger.Println("Failed to fetch NodeInfo: ", err.Error())
	}
	application_name = nodeInfo.Config.ApplicationName

	topic := application_name + "/#"
	subscriber_endpoint.Cancel()
	subscriber_endpoint = mqttclient.NewSubscriberEndpoint(topic, mqtt_msg_handler)
}

func PathsGet() []httpserver.PathInfo {
	return []httpserver.PathInfo{
		{Url: "/dash/sample", Method: httpserver.SSE, Role: rbac.USER},
		{Url: "/dash/samples", Method: httpserver.GET, Role: rbac.USER, Call: DashSamplesGet},
		{Url: "/dash/samples/last", Method: httpserver.GET, Role: rbac.USER, Call: DashSamplesGetLast},
		{Url: "/dash/stats", Method: httpserver.GET, Role: rbac.USER, Call: DashStatsGet},
	}
}

var (
	firstTimestamp      map[string]time.Time = make(map[string]time.Time)
	firstIncarnation    map[string]uint64    = make(map[string]uint64)
	firstSequenceNumber map[string]uint64    = make(map[string]uint64)
)

var (
	lastTimestamp map[string]time.Time     = make(map[string]time.Time)
	avgInterval   map[string]time.Duration = make(map[string]time.Duration)
	sampleCount   map[string]uint64        = make(map[string]uint64)
)

var mqtt_msg_handler mqtt.MessageHandler = func(_ mqtt.Client, msg mqtt.Message) {
	// start := time.Now()
	msgHandlerLock.Lock()
	defer msgHandlerLock.Unlock()
	// logger.Println("lock DURATION", time.Since(start))
	// logger.Println("get msg", msg.Topic(), string(msg.Payload()))

	// start = time.Now()
	timestamp := time.Now()
	topic := msg.Topic()

	var element_name string
	if len(topic) >= len(application_name)+1 {
		element_name = topic[len(application_name)+1:]
	} else {
		logger.Print("WARNING: unexpected topic format")
		element_name = topic
	}

	// sequence number clock test
	// ATTENTION sync issues with other elements missing sequnce number
	// if ts, ok := firstTimestamp[topic]; ok {
	// 	incarnation := gjson.Get(string(msg.Payload()), "incarnation")
	// 	sequenceNumber := gjson.Get(string(msg.Payload()), "sequence_number")
	// 	if incarnation.Exists() && sequenceNumber.Exists() {
	// 		if incarnation.Uint() == firstIncarnation[topic] && sequenceNumber.Uint() > firstSequenceNumber[topic] {
	// 			timestamp = ts.Add((time.Duration(sequenceNumber.Uint()) - time.Duration(firstSequenceNumber[topic])) * 100 * time.Millisecond)
	// 			logger.Println(topic, " first ts: ", ts, " incarnation: ", incarnation, " sequence_number: ", sequenceNumber, " new ts: ", timestamp)
	// 		} else {
	// 			logger.Println(topic, " set new first ts: ", timestamp, " incarnation: ", incarnation, " sequence_number: ", sequenceNumber)
	// 			firstTimestamp[topic] = timestamp
	// 			firstIncarnation[topic] = incarnation.Uint()
	// 			firstSequenceNumber[topic] = sequenceNumber.Uint()
	// 		}
	// 	}
	// } else {
	// 	incarnation := gjson.Get(string(msg.Payload()), "incarnation")
	// 	sequenceNumber := gjson.Get(string(msg.Payload()), "sequence_number")
	// 	if incarnation.Exists() && sequenceNumber.Exists() {
	// 		logger.Println(topic, " set first ts: ", timestamp, " incarnation: ", incarnation, " sequence_number: ", sequenceNumber)
	// 		firstTimestamp[topic] = timestamp
	// 		firstIncarnation[topic] = incarnation.Uint()
	// 		firstSequenceNumber[topic] = sequenceNumber.Uint()
	// 	}
	// }

	// smooth timestamp interval test
	// TODO improve support for non periodic samples
	// if val, ok := lastTimestamp[topic]; ok {
	// 	avgInterval[topic] = (avgInterval[topic]*time.Duration(sampleCount[topic]) + timestamp.Sub(val)) / time.Duration(sampleCount[topic]+1)
	// 	// logger.Println(topic, " DIFF: ", timestamp.Sub(val), " AVG: ", avgInterval[topic])
	// 	if sampleCount[topic] > 10 {
	// 		if timestamp.Sub(val) > avgInterval[topic]*time.Duration(300)/time.Duration(100) {
	// 			sampleCount[topic] = 0
	// 		} else if timestamp.Sub(val) < avgInterval[topic]*time.Duration(80)/time.Duration(100) || timestamp.Sub(val) > avgInterval[topic]*time.Duration(120)/time.Duration(100) {
	// 			timestamp = val.Add(avgInterval[topic])
	// 		}
	// 	}
	// }
	// lastTimestamp[topic] = timestamp
	// sampleCount[topic]++

	// source timestamp test
	// ATTENTION sync issues with different clocks
	// payloadTimestampString := gjson.Get(string(msg.Payload()), "timestamp")
	// payloadTimestamp, err := time.Parse(time.RFC3339, payloadTimestampString.String())
	// if err != nil {
	// 	// logger.Println("WARNING: error parsing payload timestamp:", err)
	// } else {
	// 	timestamp = payloadTimestamp
	// }

	sample := sample_type{Time: timestamp, Element: element_name, Message: msg.Payload()}
	// logger.Println("create sample DURATION", time.Since(start))

	// start = time.Now()
	jsonData, err := json.Marshal(sample)
	if err != nil {
		logger.Print(err)
		return
	}
	// logger.Println("marshal DURATION", time.Since(start))

	// start = time.Now()
	httpserver.SendSSE(endpoint, jsonData)
	// logger.Println("send SSE DURATION", time.Since(start))

	// add to samples buffer
	// start = time.Now()
	samplesLock.Lock()
	*samples = append(*samples, sample)
	samplesLock.Unlock()
	// logger.Println("lock and append DURATION", time.Since(start))
}

func sample_handler() {
	var conn *sql.Conn
	var err error

	for {
		conn, err = db.Conn(context.Background())
		if err != nil {
			logger.Println("SQL conn error:", err)
			time.Sleep(3 * time.Second)
			continue
		} else {
			break
		}
	}
	defer conn.Close()

	insert_counter := 0
	var average_duration time.Duration
	var max_duration time.Duration

	for {
		// logger.Println("sample_handler:", len(*samples))
		if len(*samples) > 0 {
			// get samples, create a new buffer for writing
			samplesLock.Lock()
			samplesToInsert := samples
			samples = new([]sample_type)
			samplesLock.Unlock()

			// add samples to database

			start := time.Now()

			err = conn.Raw(func(driverConn any) error {
				conn := driverConn.(*stdlib.Conn).Conn() // conn is a *pgx.Conn
				_, err := conn.CopyFrom(
					context.Background(),
					pgx.Identifier{"samples"},
					[]string{"time", "element", "message"},
					pgx.CopyFromSlice(len(*samplesToInsert), func(i int) ([]any, error) {
						return []any{(*samplesToInsert)[i].Time, (*samplesToInsert)[i].Element, (*samplesToInsert)[i].Message}, nil
					}),
				)
				return err
			})
			samplesToInsert = nil
			if err != nil {
				logger.Println("SQL conn error:", err)
				continue
			}

			insert_duration := time.Since(start)
			// logger.Println("INSERT DURATION", insert_duration)
			if average_duration == 0 {
				average_duration = insert_duration
			} else {
				average_duration = (average_duration*time.Duration(insert_counter) + insert_duration) / time.Duration(insert_counter+1)
			}
			insert_counter++
			// logger.Println("AVERAGE INSERT DURATION", average_duration)
			if insert_duration > max_duration {
				max_duration = insert_duration
			}
			// logger.Println("MAX INSERT DURATION", max_duration)
		}

		time.Sleep(db_write_interval)
	}
}

func DashSamplesGet(p periHttp.PeriHttp) {
	logger.Println("DashSamplesGet: ", p.Req().URL.Query())

	var err error
	var begin, end time.Time

	beginString := p.Req().URL.Query().Get("begin")
	endString := p.Req().URL.Query().Get("end")

	if beginString != "" || endString != "" {
		if beginString == "" {
			logger.Println("begin variable is missing")
			p.JsonResponse(http.StatusBadRequest, json.RawMessage(`{"error": "begin variable is missing"}`))
			return
		}
		begin, err = time.Parse(time.RFC3339, beginString)
		if err != nil {
			logger.Println("error parsing begin time:", err)
			p.JsonResponse(http.StatusInternalServerError, err.Error())
			return
		}
		logger.Println(begin)

		if endString == "" {
			logger.Println("end variable is missing")
			p.JsonResponse(http.StatusBadRequest, json.RawMessage(`{"error": "end variable is missing"}`))
			return
		}
		end, err = time.Parse(time.RFC3339, endString)
		if err != nil {
			logger.Println("error parsing end time:", err)
			p.JsonResponse(http.StatusInternalServerError, err.Error())
			return
		}
		logger.Println(end)

		if end.Compare(begin) == -1 {
			logger.Println("error: end time is earlier than begin time")
			p.JsonResponse(http.StatusBadRequest, json.RawMessage(`"error: end time is earlier than begin time"`))
			return
		}
	}

	limitString := p.Req().URL.Query().Get("samples")
	if limitString == "" {
		logger.Println("samples variable is missing")
		p.JsonResponse(http.StatusBadRequest, json.RawMessage(`{"error": "samples variable is missing"}`))
		return
	}
	limit, err := strconv.Atoi(limitString)
	if err != nil {
		logger.Println("error parsing samples:", err)
		p.JsonResponse(http.StatusInternalServerError, err.Error())
		return
	}
	logger.Println("samples limit: ", limit)

	perf_limit := 1000000
	logger.Println("performance limit: ", perf_limit)

	// get distinct elements

	start := time.Now()
	var rows *sql.Rows
	// Loose Index Scan emulation for postgresSQL, fast equivalent to "SELECT DISTINCT element FROM samples"
	rows, err = db.Query(`
		WITH RECURSIVE t AS (
			(SELECT element FROM samples ORDER BY element LIMIT 1)
			UNION ALL
			SELECT (SELECT element FROM samples WHERE element > t.element ORDER BY element LIMIT 1)
			FROM t
			WHERE t.element IS NOT NULL
		)
		SELECT element FROM t WHERE element IS NOT NULL;
	`)
	logger.Println("SELECT DISTINCT element DURATION", time.Since(start))
	if err != nil {
		logger.Println("SQL query error:", err)
		p.JsonResponse(http.StatusInternalServerError, err.Error())
		return
	}

	start = time.Now()
	elements := []string{}
	for rows.Next() {
		var element string
		if err := rows.Scan(&element); err != nil {
			logger.Println("SQL query error:", err)
			p.JsonResponse(http.StatusInternalServerError, err.Error())
			return
		}
		elements = append(elements, element)
	}
	if err := rows.Err(); err != nil {
		logger.Println("SQL query error:", err)
		p.JsonResponse(http.StatusInternalServerError, err.Error())
		return
	}
	rows.Close()
	logger.Println("SELECT DISTINCT element scan DURATION", time.Since(start))
	logger.Println(litter.Sdump(elements))

	// TODO idea split timestamp in multiple columns

	// type sample_type2 struct {
	// 	Time    time.Time       `json:"time"`
	// 	Element   string          `json:"element"`
	// 	Message json.RawMessage `json:"message"`
	// 	Minute  time.Time       `json:"minute"`
	// }
	// db_samples2 := []sample_type2{}
	// start2 := time.Now()
	// for _, element := range elements {
	// 	start3 := time.Now()
	// 	if begin.IsZero() && end.IsZero() {
	// 		// rows, err = db.Query("SELECT DISTINCT (element, time::date) FROM samples")
	// 		// rows, err = db.Query("SELECT DISTINCT ON(DATE_TRUNC('minute', time)) time, element, message FROM samples WHERE element=$1;", element)
	// 		rows, err = db.Query("SELECT DISTINCT ON(minute) time, element, message FROM samples WHERE element=$1;", element)
	// 	} else {
	// 		// 	rows, err = db.Query("SELECT DISTINCT ON(time::date) time, element, message FROM samples WHERE time>=$1 AND time<=$2 ORDER BY time::date;", begin.Format(time.RFC3339), end.Format(time.RFC3339))
	// 		// 	// rows, err = db.Query("SELECT DISTINCT (element, time::date) FROM samples WHERE time>=$1 AND time<=$2", begin.Format(time.RFC3339), end.Format(time.RFC3339))
	// 		rows, err = db.Query("SELECT DISTINCT ON(DATE_TRUNC('minute', time)) time, element, message FROM samples WHERE time>=$1 AND time<=$2 AND element=$3;", begin.Format(time.RFC3339), end.Format(time.RFC3339), element)
	// 	}

	// 	logger.Println(err)
	// 	logger.Println("SELECT DISTINCT ON DURATION", time.Since(start3))
	// 	start3 = time.Now()
	// 	for rows.Next() {
	// 		var sample sample_type2
	// 		if err := rows.Scan(&sample.Time, &sample.Element, &sample.Message); err != nil {
	// 			logger.Println("SQL query error:", err)
	// 			p.JsonResponse(http.StatusInternalServerError, err.Error())
	// 			return
	// 		}
	// 		db_samples2 = append(db_samples2, sample)
	// 	}
	// 	if err := rows.Err(); err != nil {
	// 		logger.Println("SQL query error:", err)
	// 		p.JsonResponse(http.StatusInternalServerError, err.Error())
	// 		return
	// 	}
	// 	rows.Close()

	// 	logger.Println("scan DURATION", element, time.Since(start3))
	// 	logger.Println("scan LENGTH", element, len(db_samples2))
	// }

	// logger.Println("scan all DURATION", time.Since(start2))
	// logger.Println("scan all LENGTH", len(db_samples2))
	// // for _, sample := range db_samples2 {
	// // 	logger.Println("sample2: ", sample.Time, sample.Minute, sample.Element, string(sample.Message))
	// // }

	// get samples

	logger.Println("DashSamplesGet: #2")
	startComplete := time.Now()
	db_samples := []sample_type{}
	for _, element := range elements {
		start = time.Now()
		var rows *sql.Rows
		if begin.IsZero() && end.IsZero() {
			logger.Println("get ALL samples")
			rows, err = db.Query("SELECT time, element, message FROM (SELECT *, row_number() over() row FROM samples WHERE element=$3 LIMIT $2) t WHERE t.row % (SELECT CEIL( COUNT(*) / $1::float )::integer FROM (SELECT * FROM samples WHERE element=$3 LIMIT $2)) = 0;", limit, perf_limit, element)
		} else {
			logger.Println("get selected samples", begin.Format(time.RFC3339), end.Format(time.RFC3339))
			rows, err = db.Query("SELECT time, element, message FROM (SELECT *, row_number() over() row FROM samples WHERE time>=$1 AND time<=$2 AND element=$5 LIMIT $4) t WHERE t.row % (SELECT CEIL( COUNT(*) / $3::float )::integer FROM (SELECT * FROM samples WHERE time>=$1 AND time<=$2 AND element=$5 LIMIT $4)) = 0;", begin.Format(time.RFC3339), end.Format(time.RFC3339), limit, perf_limit, element)
		}
		if err != nil {
			logger.Println("SQL query error:", err)
			p.JsonResponse(http.StatusInternalServerError, err.Error())
			return
		}
		select_duration := time.Since(start)
		logger.Println("SELECT DURATION", select_duration)
		logger.Println("DashSamplesGet: #3")

		start = time.Now()
		for rows.Next() {
			var sample sample_type
			if err := rows.Scan(&sample.Time, &sample.Element, &sample.Message); err != nil {
				logger.Println("SQL query error:", err)
				p.JsonResponse(http.StatusInternalServerError, err.Error())
				return
			}
			db_samples = append(db_samples, sample)
		}
		if err := rows.Err(); err != nil {
			logger.Println("SQL query error:", err)
			p.JsonResponse(http.StatusInternalServerError, err.Error())
			return
		}
		rows.Close()

		logger.Println("DashSamplesGet: #4")

		scan_duration := time.Since(start)
		logger.Println("scan DURATION", scan_duration)
		logger.Println("scan LENGTH", len(db_samples))
	}

	logger.Println("DashSamplesGet: #5")

	logger.Println("complete scan DURATION", time.Since(startComplete))

	// TODO use SQL sort, check sort on insert
	start = time.Now()
	sort.SliceStable(db_samples, func(i, j int) bool {
		return db_samples[i].Time.Before(db_samples[j].Time)
	})
	logger.Println("sort DURATION", time.Since(start))
	logger.Println("samples COUNT", len(db_samples))

	p.JsonResponse(http.StatusOK, db_samples)

	logger.Println("DashSamplesGet: end")
}

func DashSamplesGetLast(p periHttp.PeriHttp) {
	logger.Println("DashSamplesGetLast: ", p.Req().URL.Query())

	limitString := p.Req().URL.Query().Get("samples")
	if limitString == "" {
		logger.Println("samples variable is missing")
		p.JsonResponse(http.StatusBadRequest, json.RawMessage(`{"error": "samples variable is missing"}`))
		return
	}
	limit, err := strconv.Atoi(limitString)
	if err != nil {
		logger.Println("error parsing samples:", err)
		p.JsonResponse(http.StatusInternalServerError, err.Error())
		return
	}
	logger.Println("samples limit: ", limit)

	// get samples

	logger.Println("DashSamplesGetLast: #2")
	start := time.Now()
	rows, err := db.Query("SELECT time, element, message FROM samples ORDER BY time LIMIT $1 OFFSET GREATEST((SELECT COUNT(*) FROM samples) - $1, 0);", limit)
	if err != nil {
		logger.Println("SQL query error:", err)
		p.JsonResponse(http.StatusInternalServerError, err.Error())
		return
	}
	select_duration := time.Since(start)
	logger.Println("SELECT DURATION", select_duration)
	logger.Println("DashSamplesGetLast: #3")

	start = time.Now()
	db_samples := []sample_type{}
	for rows.Next() {
		var sample sample_type
		if err := rows.Scan(&sample.Time, &sample.Element, &sample.Message); err != nil {
			logger.Println("SQL query error:", err)
			p.JsonResponse(http.StatusInternalServerError, err.Error())
			return
		}
		db_samples = append(db_samples, sample)
	}
	if err := rows.Err(); err != nil {
		logger.Println("SQL query error:", err)
		p.JsonResponse(http.StatusInternalServerError, err.Error())
		return
	}
	rows.Close()

	logger.Println("DashSamplesGetLast: #4")

	scan_duration := time.Since(start)
	logger.Println("scan DURATION", scan_duration)
	logger.Println("scan LENGTH", len(db_samples))

	// for _, sample := range db_samples {
	// 	logger.Println("sample: ", sample.Time, sample.Element, string(sample.Message))
	// }

	logger.Println("DashSamplesGetLast: #5")

	p.JsonResponse(http.StatusOK, db_samples)

	logger.Println("DashSamplesGetLast: end")
}

func DashStatsGet(p periHttp.PeriHttp) {
	// get database size

	var stats stats_type

	row := db.QueryRow("SELECT pg_database_size('charts');")
	var size uint64
	err := row.Scan(&size)
	if err != nil {
		logger.Println("SQL query error:", err)
		p.JsonResponse(http.StatusInternalServerError, err.Error())
		return
	}
	// logger.Println("DATABASE size: ", size)
	stats.DBSize = size

	// get free space left

	var stat unix.Statfs_t

	unix.Statfs("/", &stat)

	// Available blocks * size per block = available space in bytes
	// logger.Println("free space left", stat.Bavail*uint64(stat.Bsize))

	stats.FreeSpace = stat.Bavail * uint64(stat.Bsize)

	// row = db.QueryRow("SELECT writebacks FROM pg_stat_io;")
	// var writebacks uint64
	// err = row.Scan(&writebacks)
	// if err != nil {
	// 	logger.Println("SQL query error:", err)
	// 	p.JsonResponse(http.StatusInternalServerError, err.Error())
	// 	return
	// }

	// rows, _ := db.Query("SELECT backend_type, writebacks, op_bytes FROM pg_stat_io;")

	// for rows.Next() {
	// 	var backend_type string
	// 	var writebacks uint64
	// 	var op_bytes uint64
	// 	if err := rows.Scan(&backend_type, &writebacks, &op_bytes); err != nil {
	// 		logger.Println("SQL query error:", err)
	// 		continue
	// 	}
	// 	logger.Println("row:", backend_type, writebacks, op_bytes)
	// }

	// logger.Println("DATABASE writebacks: ", writebacks)

	// get samples row count from last 24 hours

	// start := time.Now()
	begin := time.Now().Add(-24 * time.Hour)
	end := time.Now()
	row = db.QueryRow("SELECT COUNT(*), (SELECT pg_column_size(tr.*) FROM samples AS tr LIMIT 1) FROM (SELECT * FROM samples WHERE time>=$1 AND time<=$2);", begin.Format(time.RFC3339), end.Format(time.RFC3339))
	// logger.Println("COUNT DURATION", time.Since(start))
	var count uint64
	var avg_size uint64
	err = row.Scan(&count, &avg_size)
	if err != nil {
		logger.Print(err)
		p.JsonResponse(http.StatusInternalServerError, err.Error())
		return
	}
	write_rate := count * avg_size
	// logger.Println("DashStatsGet: SAMPLES COUNT: ", count)
	// logger.Println("DashStatsGet: SAMPLES SIZE: ", avg_size)
	// logger.Println("DashStatsGet: SAMPLES WRITE RATE PER DAY: ", write_rate)

	stats.WriteRateLastDay = write_rate

	p.JsonResponse(http.StatusOK, stats)
}
